# Pony Yummy 8-Ball
A simple "Magic 8-Ball"-type bot for Twitter. It reads every tweets from users who mentioned it. Then, it generates a non-random response by calculating the "magic" value of the question. There is currently 10 positive, 5 neutral and 5 bad responses, as the original "Magic 8-Ball" has.

# Requirements
**PY8B** has been only tested on a Python 3.4 runtime, using Tweepy at its 3.5 version. It may works on lower Python3 version but not on Python2.

# How it works (user-end)
* You have a yes-no question and absolutely need a response.
* You tweet this question to PY8Ball
* Wait for 2 minutes
* PY8Ball will calculate the "magic" value of your question and will find the response in the arcanes of its magic.

**Note:** PY8Ball only respond if you directly mention it. (The first characters **have** to be its account name)

# How to install
(TODO)

# How to use
(TODO)

# Contributors
- [Lyyn](https://bitbucket.org/Lyyn/py8ball) *(Main code, fondator)*

# License
## [Creative Commons CC BY 4.0](http://creativecommons.org/licenses/by/4.0/)  
**You are free to:**  
- **Share —** copy and redistribute the material in any medium or format  
- **Adapt —** remix, transform, and build upon the material

for any purpose, even commercially.  
The licensor cannot revoke these freedoms as long as you follow the license terms.

**Under the following terms:**  
**Attribution —** You must give ***appropriate credit***, provide a link to the license, ***and indicate if changes were made***. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.  

**No additional restrictions —** You may not apply legal terms or **technological measures** that legally restrict others from doing anything the license permits.