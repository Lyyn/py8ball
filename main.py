# -*- coding:utf-8 -*-
#
# PY8Ball
# °°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
# Pony Yummy 8-ball
# Gives you every response you need to all your existential questions.
# °°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
# Author: Lyyn
# Version: Beta (need testing)
# Dependencies :
#   Python 3.4
#   Tweepy 3.5 (via pip)
#
# Contact:
#   Twitter:    @MayakoLyyn
#   Mail:		lyynou@gmail.com
#
# Side notes:
#   Twitter limits the GET call statuses/mentions_timeline up to 15 requests
#   per 15 minutes.
#   Launching this script every 2 minutes seems fine.
#

import tweepy
import configparser
import os

# Config
CONFIG_FILE = "account.ini"     # Twitter keys (required)
QUOTES_FILES = "responses.txt"  # All possibles responses (required)
CHECK_FILE = "lastcheck.dat"    # ID of the last checked tweet (required)
ACCOUNT_NAME = "@PY8Ball"       # Your bot's Tweetname
HASHTAG = "#PY8B"               # Your bot's custom hashtag
VERBOSE = True                  # Verbose printing in the console

# -----------------------------------------------------------------------------


def loadBallz():
    # Get config info
    configFile = configparser.ConfigParser()

    if not configFile.read(CONFIG_FILE):
        print('Please create a config file.')
        exit()

    if VERBOSE: print('# Config file readed.')

    try:
        config = configFile['twitter']
        consumerKey = config['consumerKey']
        consumerSecret = config['consumerSecret']
        accessToken = config['accessToken']
        accessTokenSecret = config['accessTokenSecret']
    except KeyError:
        print('One or many keys are missing in the config file')
        exit()

    if VERBOSE: print('# Keys readed from the config file.')

    # Get quotes
    try:
        quotesFiles = open(QUOTES_FILES, 'rt', encoding='utf-8')
    except FileNotFoundError:
        print('Please create a quotes file')
        exit()
    except (OSError, IOError):
        print('The quote file were unreadable')
        exit()

    if VERBOSE: print('# Quotes file opened.')

    # Read all quotes
    quotes = []
    for string in quotesFiles:
        if not string.startswith('#') and not string == '\n':
            quotes.append(string.strip('\n'))


    # Get last tweet id
    try:
        checkFile = open(CHECK_FILE, 'rt', encoding='utf-8')
    except FileNotFoundError:
        print('Please create a check ID file with a recent id')
        exit()
    except (OSError, IOError):
        print('The check ID file were unreadable')
        exit()

    try:
        lastID = int(checkFile.readline().strip('\n'))
        checkFile.close()
    except ValueError:
        print('Incoherent value found in the check ID file')
        exit()

    if VERBOSE: print('# Check file opened.')

    # Connect to Twitter
    twAuth = tweepy.OAuthHandler(consumerKey, consumerSecret)
    twAuth.set_access_token(accessToken, accessTokenSecret)
    twApi = tweepy.API(twAuth)

    rateLimit = twApi.rate_limit_status()['resources']['statuses']['/statuses/mentions_timeline']['remaining']
    if rateLimit == 0:
        print('[WARNING] Rate limit exceeded!')
    else:
        if VERBOSE: print('[LIMIT] Remaining: ' + str(rateLimit))


    try:
        resultSet = twApi.mentions_timeline(since_id=lastID, count=200)
        resultSet.reverse()
    except tweepy.RateLimitError as e:
        print('[WARNING] Rate limit exceeded !')
        print('> ' + e.reason)
        exit()

    # Parse every new tweets
    for tweet in resultSet:
        question = normalize(tweet.text)
        value = 0
        fromUser = "@" + tweet.user.screen_name
        fromTwId = tweet.id_str
        vPrint = ""

        if question.startswith(normalize(ACCOUNT_NAME)):
            for character in question:
                try:
                    value += ord(character)
                    vPrint += character
                except Exception:
                    pass

            value %= len(quotes)
            if VERBOSE:
                print('    > Tweet: ')
                for character in vPrint:
                    try:
                        print(character, end='')
                    except Exception:
                        print('?', end='')
                print('')
                print('    > Value %: ' + str(value))

            newTweet = quotes[value] + "\n\n" + fromUser + " " + HASHTAG
            if VERBOSE: print('    > New Tweet: ' + quotes[value])

            try:
                twApi.update_status(status=newTweet, in_reply_to_status_id=fromTwId)
            except tweepy.TweepError as e:
                print('[UPDATE] Error: ' + e.reason)
            except Exception:
                pass
        else:
            if VERBOSE:
                print('# Mention found but doesn\'t starts with ' + ACCOUNT_NAME)
                for character in tweet.text:
                    try:
                        print(character, end='')
                    except Exception:
                        print('?', end='')

    # Set last parsed ID as last checked ID in the file
    if len(resultSet) > 0:
        checkFile = open(CHECK_FILE, 'wt', encoding='utf-8')
        checkFile.write(resultSet[(len(resultSet) - 1)].id_str)
        checkFile.close()
        if VERBOSE: print('# Last ID updated.')
    else:
        if VERBOSE: print('# No new mention found. Last ID keeped.')


def normalize(sentence):
    return sentence.strip().lower()



if __name__ == "__main__":
    abspath = os.path.abspath(__file__)
    dname = os.path.dirname(abspath)
    os.chdir(dname)
    loadBallz()
else:
    print("Please, don't use this script as a module. Run it through the main.py file.")